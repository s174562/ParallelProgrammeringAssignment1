Rounds: 10
Warmups: 5
File: 100-0.txt
Pattern: Something is rotten in the state of Denmark.

Setup:
Number of thread X
Number of tasks 1 - Average speedup
Number of tasks 2 - Average speedup
Number of tasks 3 - Average speedup
...
Number of tasks 10 - Average speedup

Number of threads1
1,05
0,90
1,00
1,00
1,00
1,00
1,00
1,00
1,00
1,00
Number of threads2
1,00
1,94
1,45
1,94
1,65
1,95
1,73
1,95
1,77
1,99
Number of threads3
1,00
1,95
2,17
1,75
2,05
2,17
1,94
2,09
2,17
2,06
Number of threads4
1,00
1,94
2,14
2,38
1,92
2,19
2,28
2,34
2,14
2,30
Number of threads5
1,01
1,95
2,15
2,38
2,40
2,15
2,25
2,27
2,28
2,50
Number of threads6
1,03
1,96
2,20
2,29
2,30
2,39
2,12
2,17
2,23
2,24
Number of threads7
1,00
1,72
2,08
2,31
2,19
2,35
2,35
2,24
2,21
2,36
Number of threads8
1,05
1,95
2,00
2,15
2,19
2,44
2,38
2,45
2,28
2,33
Number of threads9
1,00
1,95
2,18
2,39
2,19
2,37
2,37
2,37
2,52
2,32
Number of threads10
1,00
1,95
2,22
2,40
2,43
2,36
2,35
2,41
2,43
2,42
